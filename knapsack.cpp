//
//  knapsack.cpp
//  zeroOneknapsack
//
//  Created by Sparsh Gupta on 2/24/18.
//  Copyright © 2018 Sparsh Gupta. All rights reserved.
//

#include "knapsack.hpp"
#include<stdio.h>
int knapSack(int maxWeight, int weights[], int values[], int n)
{
    int KnapSackTable[n+1][maxWeight+1];
    for (int i = 0; i <= n; i++)
    {
        for (int w = 0; w <= maxWeight; w++)
        {
            if (i==0 || w==0)
                KnapSackTable[i][w] = 0;
            else if (weights[i-1] <= w)
                KnapSackTable[i][w] = MAX(values[i-1] + KnapSackTable[i-1][w-weights[i-1]],  KnapSackTable[i-1][w]);
            else
                KnapSackTable[i][w] = KnapSackTable[i-1][w];
        }
    }
    return KnapSackTable[n][maxWeight];
}


//
//  main.cpp
//  zeroOneknapsack
//
//  Created by Sparsh Gupta on 2/24/18.
//  Copyright © 2018 Sparsh Gupta. All rights reserved.
//

#include "knapsack.hpp"
#include <iostream>
#include <exception>
#include <sstream>
#include <string>
using namespace std;
int main(int argc, const char * argv[]) {
    int n;
    cout<<"Input number of candidates in the boolean network."<<endl;
    cin>>n;
    if (cin.good())
    {
        cout<<"Input "<<n<<" Boolean network weights."<<endl;
        string line;
        int weights[n];
        stringstream ss;
        string word;
        int count = 0;
        while(getline(cin,line) && (count!=n)){
            if (line.empty())
                continue;
            ss.str(stringstream(line).str());
            for (int i = 0; (ss >> word); i++)
            {
                if (count>=n){
                    throw runtime_error("Input exceeded number of nodes.");
                }
                size_t index = 0;
                try {
                    int number = stoi(word,&index);
                    if(index == word.length()){
                        weights[i] = number;
                    }else{
                        throw runtime_error("Not a number.");
                    }
                } catch (invalid_argument) {
                    throw runtime_error("Not a number.");
                }catch(out_of_range){
                    throw runtime_error("Not a number.");
                }
                ++count;
            }
        }
        int values[n];
        cout<<"Input corresponding Boolean network values."<<endl;
        count = 0;
        line.clear();
        word.clear();
        ss.clear();
        while(getline(cin,line)&& (count!=n)){
            if (line.empty())
                continue;
            ss.str(stringstream(line).str());
            for (int i = 0; (ss >> word); i++)
            {
                if (count>=n){
                    throw runtime_error("Input exceeded number of nodes.");
                }
                size_t index = 0;
                try {
                    int number = stoi(word,&index);
                    if(index == word.length()){
                        values[i] = number;
                    }else{
                        throw runtime_error("Not a number.");
                    }
                } catch (invalid_argument) {
                    throw runtime_error("Not a number.");
                }catch(out_of_range){
                    throw runtime_error("Not a number.");
                }
                ++count;
            }
        }
        int maxWeight;
        cout<<"Maximum weight of the boolean network."<<endl;
        cin>>maxWeight;
        cout<<"Maximum value which can be achieved for the given network = "<<knapSack(maxWeight, weights, values, n)<<endl;
    }
    else
    {
        throw runtime_error("Invalid argument, node network length can only be positive.");
    }
    return 0;
}

//
//  knapsack.hpp
//  zeroOneknapsack
//
//  Created by Sparsh Gupta on 2/24/18.
//  Copyright © 2018 Sparsh Gupta. All rights reserved.
//

#ifndef knapsack_hpp
#define knapsack_hpp

#include <stdio.h>
#define MAX(x, y) (((x) > (y)) ? (x) : (y))
int knapSack(int maxWeight, int weights[], int values[], int numberOfItems);
#endif /* knapsack_hpp */

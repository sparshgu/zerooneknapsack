zeroOneknapsack: main.o knapsack.o
	g++ -o zeroOneknapsack -g main.o knapsack.o

knapsack.o: knapsack.cpp
	g++ -g -c -Wall knapsack.cpp

main.o: main.cpp
	g++ -g -c -Wall main.cpp

clean:
	rm -f *.o zeroOneknapsack
